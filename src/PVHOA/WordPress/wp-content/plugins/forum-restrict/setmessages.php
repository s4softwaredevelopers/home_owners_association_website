<?php
$this->messages = array();
$this->messages['approved'] = "
Welcome. The forums moderator has approved your request to join the [forum_name] forum on [site_name]. 
You should be able to post to the group immediately. If there are any problems, please contact the moderator using the \"Contact Us\" link on the main page. We hope you enjoy your experience here.
";

$this->messages['denied'] = "
We are sorry to inform you that your request to join the [forum_name] forum on [site_name] has been denied. The most likely cause is that the reason you gave for joining was not sufficient. You are permitted 3 attempts to apply for membership in the forum. This is attempt number [num_attempts]. Please log in to the main site and clickon \"Site Admin\" and then look for the Forum Application link in the User menu. You can edit the reason and resubmit your application there. If you have exhausted your attempts, please contact the moderator using the \"Contact Us\" link on the main page.
";

$this->messages['reconsidered'] = "
We are happy to inform you that your formerly denied request to join the [forum_name] forum on [site_name] has been reconsidered. If you still wisth to apply, we ask that you log in to the main site and clickon \"Site Admin\" and then look for the Forum Application link in the User menu. You can edit the reason and resubmit your application.
";

$this->messages['invite'] = "
We see that you have joined [site_name] and would like to extend an invitation for you to join our [forum_name] forum. Click on the \"Apply\" link next to the forum name. If you have any questions, please contact the moderator using the \"Contact Us\" link on the main page.
";
?>
