
	jQuery( "#adminselections" ).on( "load", function() {
		jQuery(this).hide();
	});

	forum_id = 0;

jQuery(document).ready(function() {

	var adminurl = jQuery( "#adminurl" ).val();
	var usersurl = adminurl + "/users.php";

	function fadeinout(msg, obj, t) {
		obj.hide().html(msg).slideUp(1300).delay(700).fadeIn(400).fadeOut(t);
	}

	function underhanded(forum_id, bt) {
		var id = bt.attr("id");
		alert("in forum "+forum_id+" button "+id+" is not defined yet.");
	}

	function userfunctions(forum_id, bt) {
		var id = bt.attr("id");
		var parts = id.split("_");
		var func = parts[0];
		var userId = parts[1];
                jQuery.post(
			RestrictAjax.ajaxurl, {
				page: 'forum_restrict',
				userId: userId,
				forumId: forum_id,
				func: func,
                                action: 'userfunction'
                        },
                        function (response) {
				var msg = response.result.message;
				if (response.result.result) {
					switch(response.result.func) {
						case "memberdelete":
							jQuery("#nonmemberscheck").trigger("click");
							break;
						case "membersuspend":
							jQuery("#suspendedscheck").trigger("click");
							break;
						case "memberunsuspend":
							jQuery("#memberscheck").trigger("click");
							break;
						case "memberban":
							jQuery("#banscheck").trigger("click");
							break;
						case "memberunban":
							jQuery("#memberscheck").trigger("click");
							break;
						case "applicantapprove":
							jQuery("#memberscheck").trigger("click");
							break;
						case "applicantdeny":
							jQuery("#nonmemberscheck").trigger("click");
							break;
						case "applicantban":
							jQuery("#banscheck").trigger("click");
							break;
					}
				} else {
					fadeinout(msg, jQuery(" #errorcell "), 2400);
				}
                        }
		);
	}

	function buildlist(forum_id, type, lst) {
		var cnt = 0;
		jQuery.each(lst, function(index, value) { cnt++; });
		if (cnt > 0) {
			jQuery('#tabletitle').html(type);
		} else {
			fadeinout("No results", jQuery(" #errorcell "), 2400);
			return;
		}
		switch(type) {
			case "Members":
				var hrow = jQuery('<tr>').appendTo('#usertable');
				var th = jQuery('<th>').attr("class","userhead").append("User").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Display Name").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Action").appendTo(hrow);
				jQuery.each(lst, function(index, value) {
					var tr = jQuery("<tr>").appendTo('#usertable');
					var td = jQuery('<td>').attr("class","userrow").append(value.user_nicename).appendTo(tr);;
					var td = jQuery('<td>').attr("class","userrow").append(value.display_name).appendTo(tr);;
					var bt1 = jQuery('<button>', {
						text: 'Delete',
						id: 'memberdelete_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var bt2 = jQuery('<button>', {
						text: 'Suspend',
						id: 'membersuspend_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var bt3 = jQuery('<button>', {
						text: 'Ban',
						id: 'memberban_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var td = jQuery('<td>').attr("class","userrow").append(bt1).append(bt2).append(bt3).appendTo(tr);;
				});
				break;
			case "Applicants":
				var hrow = jQuery('<tr>').appendTo('#usertable');
				var th = jQuery('<th>').attr("class","userhead").append("User").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Display Name").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Reason").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Action").appendTo(hrow);
				jQuery.each(lst, function(index, value) {
					var tr = jQuery("<tr>").appendTo('#usertable');
					var td = jQuery('<td>').attr("class","userrow").append(value.user_nicename).appendTo(tr);;
					var td = jQuery('<td>').attr("class","userrow").append(value.display_name).appendTo(tr);;
					var td = jQuery('<td>').attr("class","userrow").append(value.reason).appendTo(tr);;
					var bt1 = jQuery('<button>', {
						text: 'Approve',
						id: 'applicantapprove_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var bt2 = jQuery('<button>', {
						text: 'Deny',
						id: 'applicantdeny_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var bt3 = jQuery('<button>', {
						text: 'Ban',
						id: 'applicantban_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var td = jQuery('<td>').attr("class","userrow").append(bt1).append(bt2).append(bt3).appendTo(tr);;
				});
				break;
			case "Nonmembers":
				var hrow = jQuery('<tr>').appendTo('#usertable');
				var th = jQuery('<th>').attr("class","userhead").append("User").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Display Name").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Action").appendTo(hrow);
				jQuery.each(lst, function(index, value) {
					var tr = jQuery("<tr>").appendTo('#usertable');
					var td = jQuery('<td>').attr("class","userrow").append(value.user_nicename).appendTo(tr);;
					var td = jQuery('<td>').attr("class","userrow").append(value.display_name).appendTo(tr);;
					var bt = jQuery('<button>', {
						text: 'Invite',
						id: 'nonmemberinvite_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var td = jQuery('<td>').attr("class","userrow").append(bt).appendTo(tr);;
				});
				break;
			case "Suspended":
				var hrow = jQuery('<tr>').appendTo('#usertable');
				var th = jQuery('<th>').attr("class","userhead").append("User").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Display Name").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Action").appendTo(hrow);
				jQuery.each(lst, function(index, value) {
					var tr = jQuery("<tr>").appendTo('#usertable');
					var td = jQuery('<td>').attr("class","userrow").append(value.user_nicename).appendTo(tr);;
					var td = jQuery('<td>').attr("class","userrow").append(value.display_name).appendTo(tr);;
					var bt = jQuery('<button>', {
						text: 'Unsuspend',
						id: 'memberunsuspend_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var td = jQuery('<td>').attr("class","userrow").append(bt).appendTo(tr);;
				});
				break;
			case "Banned":
				var hrow = jQuery('<tr>').appendTo('#usertable');
				var th = jQuery('<th>').attr("class","userhead").append("User").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Display Name").appendTo(hrow);
				th = jQuery('<th>').attr("class","userhead").append("Action").appendTo(hrow);
				jQuery.each(lst, function(index, value) {
					var tr = jQuery("<tr>").appendTo('#usertable');
					var td = jQuery('<td>').attr("class","userrow").append(value.user_nicename).appendTo(tr);;
					var td = jQuery('<td>').attr("class","userrow").append(value.display_name).appendTo(tr);;
					var bt = jQuery('<button>', {
						text: 'Reinstate',
						id: 'memberunban_'+index,
						click: function() { userfunctions(forum_id, jQuery(this)); }
					});
					var td = jQuery('<td>').attr("class","userrow").append(bt).appendTo(tr);;
				});
				break;
			default:
				jQuery('#tabletitle').html("");
				break;
		}
	}


	function makelist(checktype) {
		jQuery( "#statuscell" ).html("Building list").show();
		var filt = jQuery( "#adminfilter" ).val();
		jQuery("#usertable").empty();
		jQuery('#tabletitle').html("");
                jQuery.post(
			RestrictAjax.ajaxurl, {
				page: 'forum_restrict',
				forum_id: forum_id,
				checktype: checktype,
				filter: filt,
                                action: 'makelist'
                        },
                        function (response) {
				fadeinout("Done", jQuery( "#statuscell" ), 2000);
				if (response.userlist == null) {
					fadeinout(response.emptymsg, jQuery( "#errorcell" ), 2400);
				} else {
					buildlist(forum_id, response.title, response.userlist);
				}
                        }
       	        );
	}

	jQuery( "#typefiltergo" ).click(function(e) {
		e.preventDefault();
		var checked = jQuery( ".checksel:checked");
		if (!checked.val()) {
			alert("You must first select one of the three options above.");
		} else {
			makelist(checked.attr("id"));
		}
	});

	jQuery( ".checksel" ).click(function() {
		var checktype = jQuery(this).attr("id");
		makelist(checktype);
	});

	jQuery( "#admin_forum_id" ).change(function() {
		forum_id = jQuery(this).val();
		if (forum_id != 0) {
			jQuery( "#adminselections" ).show();
		} else {
			jQuery( "#adminselections" ).hide();
		}
		return false;
	});

	jQuery( "[id^=delete_appl]" ).click(function() {
		if (confirm("Are you sure?")) {
			var id = "#"+jQuery(this).attr("id");
			var data = id.split("_");
			var forum_id = data[2];
			var user_id = data[3];
			var row_id = "#approw_"+forum_id+"_"+user_id;
	                jQuery.post(
				RestrictAjax.ajaxurl, {
					page: 'forum_restrict',
					forum_id: forum_id,
					user_id: user_id,
	                                action: 'delete_appl'
	                        },
	                        function (response) {
					jQuery(row_id).hide();
	                        }
        	        );
			return false;
		} else {
			return false;
		}
	});

	jQuery( "#submit_application" ).click(function() {
		var reason = jQuery( "#application_reason" ).val();
		if (reason == "") {
			alert("You must supply a reason.");
			return false;
		}
		return true;
	});

});

