<?php
/*
Plugin Name: Forum Restrict
Plugin URI: http://www.rexgoode.com/
Description: Restrict certain forums to certain users
Author: Rex Goode
Version: 2.0
Author URI: http://www.rexgoode.com/
*/

include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 

add_action( 'init', 'forum_restrict_script_enqueuer' );
function forum_restrict_script_enqueuer() {
        wp_register_script( "forrestrict", plugins_url("forum_restrict.js", __FILE__), array('jquery') );
	wp_localize_script('forrestrict', 'RestrictAjax', array( 'ajaxurl' =>  admin_url( 'admin-ajax.php' ) ) );
        wp_enqueue_script( 'forrestrict', plugin_dir_url(__FILE__ ) . "forum_restrict.js", array('jquery'));
}


function forum_restrict_css() {
?>
	<style type="text/css">

	#errorcell {
		font-size: 24pt;
		background-color: #fdecac;
		color: #800000;
	}

	#statuscell {
		font-size: 14pt;
		background-colors: #708090;
		color: #0000cd;
	}

	tr.userrow {
		font-size: 10pt;
		padding: 0 18px 0 19px;
	}

	th.userhead {
		font-size: 11pt;
		font-weight: bold;
		padding: 4px;
		background-color: #ffccbb;
	}

	#adminselections {
		text-align: center;
		display: none;
	}

	.forum_restrict {
		font-size: 8pt;
	}

	.forum_application {
		margin: 8pt 160pt 8pt 160pt;
		font-size: 11pt;
		line-height: 12pt;
		background-color: #dddddd;
	}
	.forum_restrict a{
		text-decoration: none;
		font-size: 10pt;
		font-weight: bold;
		color: black;
		background-color: #fee;
		margin-right: 8pt;
	}

	.forum_restrict a:hover {
		background-color: #eef;
	}

	.member_table tr td {
		padding: 0pt 6pt 0pt 6pt;
	}

	.member_table tr th {
		font-size: 12pt;
		padding: 0pt 6pt 0pt 6pt;
	}

	.forum_restrict_info {
		font-size: 12pt;
		background-color: #cdd;
		text-align: center;
	}
	</style>
<?php
}


	$adminurl = get_option('siteurl').'/wp-admin';
	$sep = (strpos(__FILE__,'/')===false)?'\\':'/';
	$WPpluggable = substr( dirname(__FILE__),0,strpos(dirname(__FILE__),'wp-content')) . 'wp-includes'.$sep.'pluggable.php';
	if ( file_exists($WPpluggable) )
		require_once($WPpluggable);

if (!class_exists("fr_Forum")) {
	class fr_Forum {
		var $forum_id;
		var $forum_name;
		var $forum_slug;
		var $restricted;
		var $userRequestable;

		function __construct($forum_id) {
			$this->forum_id = $forum_id;
			$this->read();
		}

		function read() {
			global $wpdb;

			$row = $wpdb->get_row("SELECT post_title, post_name FROM {$wpdb->prefix}posts WHERE ID = $this->forum_id", OBJECT);
			$this->forum_name = $row->post_title;
			$this->forum_slug = $row->post_name;
			list($this->restricted) = get_post_meta($this->forum_id, "isForumRestricted");
			list($this->userRequestable) = get_post_meta($this->forum_id, "userRequestable");
			list($this->isForumNonmemberInvisible) = get_post_meta($this->forum_id, "isForumNonmemberInvisible");
		}
	}
} else {
	die("fr_Forum class already exists");
}

	//Set Up Plugin Class
if (!class_exists("Forum_Restrict")) {		

    	class Forum_Restrict {
		var $pluginurl;
		var $working_forum;
		var $search_term;
		var $bbpress;
		var $messages;

		function Forum_Restrict() { //constructor
			global $user_ID;
			add_action('admin_notices', array($this,'notices'));
			$this->pluginurl = get_option('siteurl').'/wp-content/plugins/forum_restrict';
			if (isset($_REQUEST['fr_action']))
			switch($_REQUEST['fr_action']) {
				case "Submit Application":
					$forum = new fr_Forum($_REQUEST['forum_id']);
					$forum->read();
					$slug = $forum->forum_slug;
					$reason = $_REQUEST['application_reason'];
					list($previous_status) = get_user_meta($_REQUEST['user_id'], $slug."-application-status");
					if ($previous_status == "") $previous_status = "Pending";
					switch($previous_status) {
						case "Pending":
							break;
						case "Denied":
							list($apply_attempts) = get_user_meta($_REQUEST['user_id'], $slug."-reapplication-attempts");
							if (!$apply_attempts) $apply_attempts = 0;
							$apply_attempts++;
							if ($apply_attempts <= 3) $new_status = "Pending"; else $new_status = "Denied";
							update_user_meta($_REQUEST['user_id'], $slug."-application-status", $new_status);
							update_user_meta($_REQUEST['user_id'], $slug."-reapplication-attempts", $apply_attempts);
							unset($_REQUEST['forum_id']);
							break;
					}
					update_user_meta($_REQUEST['user_id'], $slug."-application-status", "Pending");
					update_user_meta($_REQUEST['user_id'], $slug."-application-reason", $reason);
					break;
			}
			$this->setmessages();
		}

		function setmessages() {
			include_once("setmessages.php");
		}

		function notices() {
			$this->bbpress = is_plugin_active("bbpress/bbpress.php");
			if (!$this->bbpress) {
				echo "<div class='error'>
				<h2>Forum Restrict is intended to work exclusively with sites with the bbpress plugin activited.
				You must install and activate bbpress plugin for this plugin to work.</h2>
				</div>";
				deactivate_plugins("forum_restrict/index.php");
			}
		}

		function Main() {
			global $wpdb, $adminurl;
		}

		function frout($str) {
			echo("$str\n");
		}

		function forum_prelim() {
			global $adminurl;
			echo "<input type=hidden id=adminurl value='$adminurl'>\n";
			echo "<script lang=javascript href='forum_restrict.js' />\n";
		}

		function administer() {
			global $wpdb, $adminurl, $pluginurl;
			echo "<h1 align=center>Forum Restrictions Administration</h1>\n";
			$this->forum_dropdown();
		}

		function user_applications() {
			global $userdata, $adminurl, $wpdb;
			echo "<br /><h1 align=center>Forum Application</h1>\n";
			$username = $userdata->data->display_name;
			$user_ID = $userdata->data->ID;
			echo "<h2 align=center>$username</h2>\n";

			if ($_REQUEST['forum_id'] && ($_REQUEST['fr_action'] == "" || $_REQUEST['fr_action'] == "User Manage")) {
				echo "<h3 align=center>$forum->forum_name</h3>\n";
				$apply_forum = new fr_Forum($_REQUEST['forum_id']);
				$apply_forum->read();
				$slug = $apply_forum->forum_slug;
				list($app_reason) = get_user_meta($user_ID, $slug."-application-reason");
				list($app_status) = get_user_meta($user_ID, $slug."-application-status");
				if ($app_status == "") $app_status = "Pending";
				list($apply_attempts) = get_user_meta($user_ID, $slug."-reapplication-attempts");
				if ($apply_attempts == "") $apply_attempts = 0;
				$apply_attempts++;
				if ($apply_attempts <= 3) {
					echo "<div class='forum_application'>\n";
					echo "<form method='post' id='apply_forum' action='$adminurl/users.php?page=forum_application'>\n";
					echo "<input type='hidden' name='forum_id' value='$_REQUEST[forum_id]' />\n";
					echo "<input type='hidden' name='user_id' value='$user_ID' />\n";
					echo "<p align=center>Your current status is <b>$app_status</b>.</p>\n";
					echo "<p align=center>In the box below, enter your reason for wanting to join this forum:</p>\n";
					echo "<p align=center><textarea rows='10' cols='50' id='application_reason' name='application_reason'>$app_reason</textarea></p>\n";
					switch($app_status) {
						case 'Pending':
							break;
						case 'Denied':
							echo "<p>You are currently in <b>Denied</b> status. This is likely because a forum moderator
	                                                         did not feel your reason was adequate for joining the forum. Each user is given
	                                                         three attempts to successfully apply for membership to a forum. For this forum,
	                                                         you are on attempt number <em>$apply_attempts</em>.";
							if ($apply_attempts == "3") echo " This is your last attempt. If you want to be reconsidered,
									you must contact the forum administrator.";
							echo "</p>\n";
							break;
						default:
							break;
					}
					echo "<center><input type=submit id='submit_application' name=fr_action value='Submit Application'>";
					echo "<input type=submit name=fr_action value='Cancel'></center>\n";
				} else {
					echo "<h2 align=center>You may no longer try to apply for membership to this forum. Please contact
						the administrator to try again.</h2>\n";
				}
				echo "</form>\n";
				echo "</div>\n";
			} else {
?>
<script type='text/javascript'>
function fr_delcheck(f) {
	var result = confirm('Are you sure?');
	if (result) {
		f.submit();
		return true;
	} else {
		return false;
	}
}
</script>
<?php
				$forum_ids = $wpdb->get_col("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type = 'forum'");
				$header = "<h3 align=center>Your Application(s)</h3>\n";
				echo "<form method='post' id='my_applications' action='$adminurl/users.php?page=forum_application'>\n";
				echo "<input type='hidden' name='fr_action' value='User Manage'>\n";
				$header .= "<table align=center class='member_table'>\n";
				$header .= "<tr><th>Forum</th><th>Reason</th><th>Status</th><th>Delete</th><th>Edit</th></tr>\n";
				foreach($forum_ids as $forum_id) {
					$forum = new fr_Forum($forum_id);
					$forum->read();
					$slug = $forum->forum_slug;
					list($app_reason) = get_user_meta($user_ID, $slug."-application-reason");
					list($app_status) = get_user_meta($user_ID, $slug."-application-status");
					list($apply_attempts) = get_user_meta($user_ID, $slug."-reapplication-attempts");
					if ($apply_attempts == "") $apply_attempts = 0;
					$apply_attempts++;
					if ($app_status == "" || $app_status == "Pending") {
						$app_status = "Pending";
						$action = "None";
					}
					if ($app_reason) {
						echo "<tr><td colspan>$header;</td></tr>\n";
						$header = "";
						echo "<tr id='approw_${forum_id}_{$user_ID}'>\n";
						echo "		<td>\n";
						echo "			$forum->forum_name\n";
						echo "		</td>\n";
						echo "		<td>\n";
						echo "			$app_reason\n";
						echo "		</td>\n";
						echo "		<td>\n";
						if ($app_status == "Denied" && $apply_attempts > 3) echo "Permanently";
						echo "			$app_status\n";

						echo "		</td>\n";
						echo "		<td align='center'>\n";
						echo "			<input type='checkbox' id='delete_appl_${forum_id}_{$user_ID}' />\n";
						echo "		</td>\n";
						echo "		<td align='center'>\n";
						if ($apply_attempts <= 3)
						echo "			<input type='checkbox' name='forum_id' value='$forum_id' onClick='return this.form.submit();' />\n";
						else echo "X";
						echo "		</td>\n";
						echo "</tr>\n";
					}
				}
				echo "</table>\n";
				echo "</form>\n";
			}
		}

		function ConfigureMenu() {
			add_submenu_page( "users.php", "Forum Restrictions Admin", "Forum Restrictions", "publish_forums", "forum_restrict", array($this, 'administer'));
			add_submenu_page( "users.php", "Forum Application", "Forum Application", "read", "forum_application", array($this, 'user_applications'));
		}

		function forum_dropdown() {
			global $wpdb;


			$forum_ids = $wpdb->get_col("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type = 'forum'");
			$forums = array();
			foreach($forum_ids as $forum_id) {
				$forum = new fr_Forum($forum_id);
				$forum->read();
				if ($forum->restricted) {
					array_push($forums, $forum);
				}
			}
			echo "<div id=\"SELECT_FORM\">\n";
			echo "<form>\n";
			if (sizeof($forums) == 0) {
				echo "<h3 align=center>\n";
				echo "	No restricted forums for this site.\n";
				echo "</h3>\n";
			} else {
				echo "<p align='center'><b>Forum:</b> "; 
				echo "<select id='admin_forum_id' name='forum_id'>\n";
				echo "<option value='0'>Choose a forum</option>\n";
				foreach($forums as $forum) {
					if ($_REQUEST['forum_id'] == $forum->forum_id) $sel = " selected"; else $sel = "";
					echo "<option value='$forum->forum_id'$sel>$forum->forum_name </option>\n";
				}
				echo "</select></p>\n";
				echo "<div><table align='center'><tr><td id='statuscell'></td></tr></table></div>\n";
				echo "<div id='adminselections'>\n";
				echo "	<input id='memberscheck' class='checksel' name=usercheck type='radio'>Members</input>\n";
				echo "	<input id='applicantscheck' class='checksel' name=usercheck type='radio'>Applicants</input>\n";
				echo "	<input id='nonmemberscheck' class='checksel' name=usercheck type='radio'>Non-members</input>\n";
				echo "	<input id='suspendedscheck' class='checksel' name=usercheck type='radio'>Suspended</input>\n";
				echo "	<input id='banscheck' class='checksel' name=usercheck type='radio'>Banned</input>\n";
				echo "	<br>\n";
				echo "	Filter: <input id='adminfilter' /> <button id='typefiltergo' name='typefiltergo'>Filter</button>\n";
				echo "</div>\n";
				echo "</form>\n";
				echo "<div><table align='center'><tr><td id='errorcell'></td></tr></table></div>\n";
				echo "<div id=\"Member_List\"><h2 id='tabletitle' align=center></h2><table id='usertable' align=center></table></div>\n";
			}
		}

		function metabox($postID) {
			$forum = new fr_Forum($postID);
			$forum->read();
			echo "<table>\n";
			echo "<tr>\n";
			echo "	<td valign=top>\n";
			if ($forum->restricted) $checked = "checked"; else $checked = "";
			echo "		<input type='checkbox' name='isForumRestricted' $checked/>\n";
			echo "	</td>\n";
			echo "	<td>\n";
			echo "		Restrict access to this forum using Forum Restrict plugin.\n";
			if ($forum->userRequestable) $checked = "checked"; else $checked = "";
			echo "	</td>\n";
			echo "</tr>\n";
			echo "<tr>\n";
			echo "	<td valign=top>\n";
			echo "		<input type='checkbox' name='isForumUserRequestable' $checked/>\n";
			echo "	</td>\n";
			echo "	<td>\n";
			echo "	Users can request to join this forum.\n";
			echo "	</td>\n";
			echo "</tr>\n";
			if ($forum->isForumNonmemberInvisible) $checked = "checked"; else $checked = "";
			echo "<tr>\n";
			echo "	<td valign=top>\n";
			echo "		<input type='checkbox' name='isForumNonmemberInvisible' $checked/>\n";
			echo "	</td>\n";
			echo "	<td>\n";
			echo "	Not visible to non-members.\n";
			echo "	</td>\n";
			echo "</table>\n";
		}

		function topic_permalink($lnk, $topic_id) {
			global $post, $user_ID;
			$forum_id = $post->post_parent;
			$forum = new fr_Forum($forum_id);
			list($ismember) = get_user_meta($user_ID, $forum->forum_slug."-member", '');
			if ($ismember) echo "$lnk"; else echo "";
		}

		function forum_title($ttitle) {
			global $post, $user_ID, $adminurl;

			$forum_id = $post->ID;
			$forum = new fr_Forum($forum_id);
			$forum->read();
			if ($user_ID) {
				list($ismember) = get_user_meta($user_ID, $forum->forum_slug."-member", '');
			} else {
				$ismember = 0;
			}

			if ($forum->restricted) {
				if ($forum->isForumNonmemberInvisible && !$ismember) return "";
				if ($forum->userRequestable) {
					$apply_link = " <a href=$adminurl/users.php?page=forum_application&forum_id=$forum_id>Apply</a>\n";
					if ($ismember) return "$ttitle"; else return "</a><em>You are not a member of $ttitle$apply_link.</em><a>";
				} else {
					return "$ttitle";
				}
			} else {
				return "$ttitle";
			}
		}

		function forum_permalink($lnk) {
			global $post, $user_ID;
			$forum_id = $post->post_parent;
			$forum = new fr_Forum($forum_id);
			list($ismember) = get_user_meta($user_ID, $forum->forum_slug."-member", '');
			if ($ismember) echo "$lnk"; else echo "";
		}

		function topic_title($ttitle) {
			global $post, $user_ID;
			$forum_id = $post->post_parent;
			$forum = new fr_Forum($forum_id);
			$forum->read();
			if ($forum->restricted) {
				list($ismember) = get_user_meta($user_ID, $forum->forum_slug."-member", '');
				if ($ismember) return "$ttitle"; else return "</a><em>Topic not available</em><a>";
			} else return $ttitle;
		}

		function set_working_forum($forum_id) {
			global $wpdb;
			if ($forum_id) {
				$this->working_forum[forum_id] = $forum_id;
				$row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}posts WHERE ID = $forum_id", OBJECT);
				if ($row) {
					$this->working_forum[forum_name] = $row->post_title;
					$this->working_forum[metaslug] = "{$row->post_name}-member";
					$this->working_forum[reasonslug] = "{$row->post_name}-application-reason";
					$this->working_forum[statusslug] = "{$row->post_name}-application-status";
					$this->working_forum[memstatusslug] = "{$row->post_name}-member-status";
				}
			}
		}

		function userlist($l, $filt) {
			global $wpdb;
			$forum = new fr_Forum($_REQUEST['forum_id']);
			$userlist = array();
			if (sizeof($l)) {
				foreach($l as $user) {
					if (preg_match("/$filt/", $user->user_nicename) || preg_match("/$filt/", $user->display_name)) {
						list($reason) = get_user_meta($user->ID, $forum->forum_slug."-application-reason");
						$reason = preg_replace("/\n/", "<br>\n", $reason);
						$reason = preg_replace("/ /", "\n", $reason);
						$userId = $user->ID;
						$userlist[$user->ID][user_nicename] = $user->user_nicename;
						$userlist[$user->ID][display_name] = $user->display_name;
						$userlist[$user->ID][reason] = $reason;
					}
				}
			}
			return $userlist;
		}

		function makelist() {
			global $wpdb;

			$result = "\n";
			$forum_id = $_REQUEST[forum_id];
			$this->set_working_forum($forum_id);
			$filt = $_REQUEST[filter];
			$slug = $this->working_forum[metaslug];
			$appslug = $this->working_forum[reasonslug];
			$statslug = $this->working_forum[statusslug];
			$memstatslug = $this->working_forum[memstatusslug];
			$blog_id = get_current_blog_id();	
			$members = get_users(array('blog_id' => $blog_id, 'meta_key' => $slug, 'meta_value' => 'true'));
			$applicants = get_users(array('blog_id' => $blog_id, 'meta_key' => $statslug, 'meta_value' => 'Pending'));
			$bans = get_users(array('blog_id' => $blog_id, 'meta_key' => $statslug, 'meta_value' => 'Banned'));
			$suspends = get_users(array('blog_id' => $blog_id, 'meta_key' => $memstatslug, 'meta_value' => 'Suspended'));
			$nonmembers = get_users(array('blog_id' => $blog_id));
			foreach($nonmembers as $ix => $non) {
				foreach($members as $mem) {
					if ($mem->ID == $non->ID) {
						unset($nonmembers[$ix]);
					}
				}
				foreach($applicants as $app) {
					if ($app->ID == $non->ID) {
						unset($nonmembers[$ix]);
					}
				}
				foreach($bans as $ban) {
					if ($ban->ID == $non->ID) {
						unset($nonmembers[$ix]);
					}
				}
				foreach($suspends as $sus) {
					if ($sus->ID == $non->ID) {
						unset($nonmembers[$ix]);
					}
				}
			}

			foreach($members as $ix => $mem) {
				foreach($suspends as $sus) {
					if ($sus->ID == $mem->ID) {
						unset($members[$ix]);
					}
				}
			}

			switch($_REQUEST[checktype]) {
				case "memberscheck":
					$title = "Members";
					$userlist = $this->userlist($members, $filt);
					$emptymsg = "No members fit the criteria.";
					break;
				case "applicantscheck":
					$title = "Applicants";
					$userlist = $this->userlist($applicants, $filt);
					$emptymsg = "No applicants fit the criteria.";
					break;
				case "suspendedscheck":
					$title = "Suspended";
					$userlist = $this->userlist($suspends, $filt);
					$emptymsg = "No users fit the criteria.";
					break;
				case "banscheck":
					$title = "Banned";
					$userlist = $this->userlist($bans, $filt);
					$emptymsg = "No users fit the criteria.";
					break;
				case "nonmemberscheck":
					$title = "Nonmembers";
					$userlist = $this->userlist($nonmembers, $filt);
					$emptymsg = "No nonmembers fit the criteria.";
					break;
			}

			$result .= "\nfilter: $_REQUEST[filter]\n";
			header("Content-type: application/json");
			echo json_encode( array ('result' => $result, 'emptymsg' => $emptymsg, 'title' => $title, 'userlist' => $userlist ));
			exit;
		}

		function make_message($status, $values) {
			$basemessage = $this->messages[$status];
			foreach($values as $tag => $value) {
				$pattern = "/\[$tag\]/";
				$basemessage = preg_replace( $pattern, $value, $basemessage);
			}
			return $basemessage; // Check for null before sending mail
		}

		function delete_appl() {
			$forum = new fr_Forum($_REQUEST['forum_id']);
			$slug = $forum->forum_slug;
			delete_user_meta($_REQUEST['user_id'], $slug."-application-status");
			delete_user_meta($_REQUEST['user_id'], $slug."-application-reason");
			delete_user_meta($_REQUEST['user_id'], $slug."-reapplication-attempts");
			header("Content-type: application/json");
			echo json_encode( array ('success' => true ));
			exit;
		}

		function save_forum($postID) {
			$forum = new fr_Forum($postID);
			$forum->read();
			$result = update_post_meta($postID, "isForumRestricted", $_REQUEST['isForumRestricted']);
			$result = update_post_meta($postID, "userRequestable", $_REQUEST['isForumUserRequestable']);
			$result = update_post_meta($postID, "isForumNonmemberInvisible", $_REQUEST['isForumNonmemberInvisible']);
		}

		function save_application() {
			global $wpdb;
			$forum = new fr_Forum($_REQUEST['forum_id']);
			$forum->read();
			$slug = $forum->forum_slug;
			$reason = $_REQUEST['application_reason'];
			list($previous_status) = get_user_meta($_REQUEST['user_id'], $slug."-application-status");
			if ($previous_status == "") $previous_status = "Pending";
			switch($previous_status) {
				case "Pending":
					break;
				case "Denied":
					list($apply_attempts) = get_user_meta($_REQUEST['user_id'], $slug."-reapplication-attempts");
					if (!$apply_attempts) $apply_attempts = 0;
					$apply_attempts++;
					if ($apply_attempts <= 3) $new_status = "Pending"; else $new_status = "Denied";
					update_user_meta($_REQUEST['user_id'], $slug."-application-status", $new_status);
					update_user_meta($_REQUEST['user_id'], $slug."-reapplication-attempts", $apply_attempts);
					unset($_REQUEST['forum_id']);
					break;
			}
			update_user_meta($_REQUEST['user_id'], $slug."-application-status", "Pending");
			update_user_meta($_REQUEST['user_id'], $slug."-application-reason", $reason);
			header("Content-type: application/json");
			echo json_encode( array ('success' => true ));
			exit;
		}

		function userfunction() {
			global $current_site, $wpdb;

			$result = array();
                        $site_name = $current_site->site_name;
			$result[message] = "";
			$result[passed] = var_export($_REQUEST, TRUE);
			$result[func] = $_REQUEST[func];
			$result[debug] = "";
			$forum = new fr_Forum($_REQUEST[forumId]);
			$result[debug] = var_export($_REQUEST, TRUE);
			$userId = $_REQUEST[userId];
			$mkey = $wpdb->prefix."capabilities";
			$usercap = get_user_meta($userId, $mkey);
			foreach($usercap as $u) {
				foreach($u as $cap => $value) {
					if (preg_match("/^bbp_/", $cap)) {
						$role = preg_replace("/^bbp_/", "", $cap);
					}
				}
			}

			$forum->read();
			$this->set_working_forum($_REQUEST['forumId']);
			$slug = $forum->forum_slug."-member";
			switch($_REQUEST[func]) {
				case "memberdelete":
					$result[message] = $userId." ".$slug;
					$result[result] = delete_user_meta($userId, $slug);
					if ($result[result]) {
						$result[message] = "Success";
					} else {
						$result[message] = "Failed to delete member.";
					}
					break;
				case "memberunban":
					$result[message] = $userId." ".$slug;
					$memresult = add_user_meta($userId, $forum->forum_slug."-member" , 'true');
					$ubanresult = delete_user_meta($userId, $forum->forum_slug."-application-status", "Banned");
					if ($memresult && $ubanresult) $result[result] = true; else $result[result] = false;
					if ($result[result]) {
						$result[message] = "Success";
					} else {
						$result[message] = "Failed to reinstate member.";
					}
					break;
				case "memberban":
					$result[message] = $userId." ".$slug;
					$delresult = delete_user_meta($userId, $slug);
					$banresult = add_user_meta($userId, $forum->forum_slug."-application-status", "Banned");
					if ($delresult && $banresult) $result[result] = true; else $result[result] = false;
					if ($result[result]) {
						$result[message] = "Success";
					} else {
						$result[message] = "Failed to ban member.";
					}
					break;
				case "membersuspend":
					if ($role == "participant") {
						unset($usercap['bbp_participant']);
						$usercap['bbp_spectator'] = true;
						$specresult = update_user_meta($userId, $mkey, $usercap);
						$susresult = add_user_meta($userId, $forum->forum_slug."-member-status", "Suspended");
						if (!$susresult) {
							$result[result] = FALSE;
							$result[message] = "Failed to suspend member.";
						} else {
							$result[result] = TRUE;
							$result[message] = "Success";
						}
					} else {
						$result[result] = FALSE;
						$result[message] = "You can only suspend participants. This user is a '$role'.";
					}
					break;
				case "memberunsuspend":
					if ($role == "spectator") {
						unset($usercap['bbp_spectator']);
						$usercap['bbp_participant'] = TRUE;
						$specresult = update_user_meta($userId, $mkey, $usercap);
					}
					$delresult = delete_user_meta($userId, $forum->forum_slug."-member-status", "Suspended");
					if (!$specresult || !$delresult) $result[result] = FALSEl; else $result[result] = TRUE;
					if ($result[result]) {
						$result[message] = "Success";
					} else {
						$result[message] = "Failed to unsuspend member.";
					}
					break;
				case "applicantapprove":
					$result[result] = update_user_meta($userId, $forum->forum_slug."-member", "true");
					if ($result[result]) {
						$message = $this->make_message("approved",
							array(
								"forum_name" => $forum->forum_name,
								"site_name" => $site_name
							)
						);
						if ($message)
							wp_mail($userdata->user_email, "Your Request Has Been Approved", $message);
						delete_user_meta($userId, $forum->forum_slug."-application-status");
						delete_user_meta($userId, $forum->forum_slug."-application-reason");
						delete_user_meta($userId, $forum->forum_slug."-reapplication-attempts");
						$result[message] = "Success";
					} else {
						$result[message] = "Failed to approve applicant.";
					}
					break;
				case "applicantdeny":
					list($attemtps) = get_user_meta($userId, $forum->forum_slug."-reapplication-attempts");
					if ($attempts == "") $attempts = 0;
					$attempts++;
					$result = update_user_meta($userId, $forum->forum_slug."-application-status", "Denied");
					$result[result] = update_user_meta($userId, $forum->forum_slug."-attemptslication-attempts", "$attempts");
					if ($result[result]) {
						$message = $this->make_message("denied",
							array(
								"forum_name" => $forum->forum_name,
								"site_name" => $site_name,
								"num_attempts" => "$attempts"
							)
						);
						if ($message)
							wp_mail($userdata->user_email, "Your Request Has Been Denied", $message);
						$result[message] = "Success";
					} else {
						$result[message] = "Failed to deny application.";
					}
					break;
				case "applicantban":
					$result[result] = update_user_meta($userId, $forum->forum_slug."-application-status", "Banned");
					if ($result[result]) {
						$result[message] = "Success";
					} else {
						$result[message] = "Failed to ban applicant.";
					}
				case "nonmemberinvite":
					$result[result] = true;
					if ($result[result]) {
						$message = $this->make_message("invite",
							array(
								"forum_name" => $forum->forum_name,
								"site_name" => $site_name
							)
						);
						if ($message)
							wp_mail($userdata->user_email, "An Invitation from $site_name", $message);
						$result[message] = "Success";
					} else {
						$result[message] = "Failed to invite nonmember.";
					}
				default:
					$result[result] = "Unknown operation";
					$result[message] = "Unknown operation";
					break;
			}
			header("Content-type: application/json");
			echo json_encode( array ('result' => $result ));
			exit;
			
		}

		function show_suspended() {
			global $user_ID, $post;
			$forum_id = $post->ID;
			$this->set_working_forum($forum_id);
			$slug = $this->working_forum[memstatusslug];
			$status = get_user_meta($user_ID, $slug);
			if ($status) {
				echo "<p align=center><b>You have been suspended. If you have any questions, please contact the forum administrator. While you are suspended, you may read but not post.</p>\n";
			}
		}
       	}
}



//Create new instance of class
if (class_exists("Forum_Restrict")) {
	$forum_restrict = new Forum_Restrict();
}

//Actions and Filters
if (isset($forum_restrict)) {
	do_action( 'wp_ajax_nopriv_' . $_REQUEST['action']);
	do_action( 'wp_ajax_' . $_POST['action']);

	add_action( 'wp_ajax_nopriv_delete_appl', array($forum_restrict, 'delete_appl'));
	add_action( 'wp_ajax_delete_appl', array($forum_restrict, 'delete_appl'));

	add_action( 'wp_ajax_nopriv_save_application', array($forum_restrict, 'save_application'));
	add_action( 'wp_ajax_save_application', array($forum_restrict, 'save_application'));

	add_action( 'wp_ajax_nopriv_makelist', array($forum_restrict, 'makelist'));
	add_action( 'wp_ajax_makelist', array($forum_restrict, 'makelist'));

	add_action( 'wp_ajax_nopriv_userfunction', array($forum_restrict, 'userfunction'));
	add_action( 'wp_ajax_userfunction', array($forum_restrict, 'userfunction'));

	add_action('admin_head', 'forum_restrict_css');
	add_action('admin_head', array($forum_restrict, 'forum_prelim'));
	add_action('admin_menu',       array($forum_restrict,'ConfigureMenu'));
	add_action('save_post', array($forum_restrict,'save_forum'));
	add_action('bbp_forum_metabox', array($forum_restrict,'metabox'));
	add_filter('bbp_get_topic_title', array($forum_restrict,'topic_title'));
	add_filter('bbp_topic_permalink', array($forum_restrict,'topic_permalink'), 10, 2);
	add_filter('bbp_get_forum_title', array($forum_restrict,'forum_title'));
	add_filter('bbp_forum_permalink', array($forum_restrict,'forum_permalink'), 10, 1);
	add_action('bbp_template_before_topics_loop', array($forum_restrict,'show_suspended'), 10, 3);
}

?>
